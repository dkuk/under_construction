## Under Construction

#### Plugin for Redmine

Plugin for Redmine that disables all or some of the choosen pages except account(login-logout) for non-admin users while technical operations runs.
You can set technical operations period and additional message to users.
[site]: http://rmplus.pro/en/redmine/plugins/under_construction

Know more on the [plugin page][site].

#### Copyright
Copyright (c) 2011-2013 Vladimir Pitin, Danil Kukhlevskiy.
[skin]: https://github.com/tdvsdv/redmine_alex_skin
For better appearance we recommend to use this plugin with Redmine skin of our team - [Redmine Alex Skin][skin].

Another plugins of our team you can see on site http://rmplus.pro

2.0.6:
 * Fixed: mobile view
2.0.5:
 * support redmine 3.4.1
2.0.4:
 * fixed: fix minor bugs
2.0.3:
 * fixed: format time in notification
2.0.2:
 * support select2 4.0.3
2.0.1:
 * refactored interface