RMPlus.Tech = (function (my) {
  var my = my || {};

  my.show_warn_if_needed = function () {
    if (typeof uc_warn_hash != 'undefined' && localStorage['uc_warn_hash'] != uc_warn_hash) {
        localStorage['uc_warn_hash'] = uc_warn_hash;
        localStorage['uc_warn_state'] = 'open';
    }

    if (localStorage['uc_warn_state'] != 'closed') {
      var warn_note = $('#uc_warn_note');
      var uc_warn_width = warn_note.outerWidth();
      warn_note.css('width', warn_note.width()+'px');
      warn_note.css('height', warn_note.height()+'px');
      warn_note.show();
      warn_note.effect('slide', {direction: 'up'} , 300, function () { $('#uc_warn_note').css('width', 'auto').css('height', 'auto')});
    }
  };

  return my;
})(RMPlus.Tech || {});


$(document).ready(function () {
  $('a.uc_preview').click(function () {
    $.ajax({ url: '/uc_periods/preview',
             dataType: 'script',
              type: 'post',
              data: {preview_text: $('#'+$(this).attr('data-obj')).val()} });

  });

  $('#uc_period_notify').change(function() {
    var $uc_no_time = $('#uc_no_time');
    if (!this.checked) {
      var ch = $uc_no_time.prop('checked');
      $uc_no_time.prop('checked', false);

      if (ch) {
        $uc_no_time.trigger('change');
      }
    }
    $uc_no_time.prop('disabled', !this.checked);
  }).trigger('change');

  $('#uc_no_time').change(function() {
    if (this.checked) {
      $('select[name^="uc_period[begin_date("], select[name^="uc_period[end_date("]').prepend('<option value="">---</option>').val('').find('option[value!=""]').hide();
      $('#uc_period_turned_on').prop('checked', false).prop('disabled', true);
    } else {
      var $dates = $('select[name^="uc_period[begin_date("], select[name^="uc_period[end_date("]');
      $dates.find('option[value=""]').remove();
      $dates.find('option[value!=""]').show();

      var dt = new Date();
      $('#uc_period_begin_date_3i, #uc_period_end_date_3i').val(dt.getDate());
      $('#uc_period_begin_date_2i, #uc_period_end_date_2i').val(dt.getMonth() + 1);
      $('#uc_period_begin_date_1i, #uc_period_end_date_1i').val(dt.getFullYear());
      var tmp = dt.getHours();
      $('#uc_period_begin_date_4i, #uc_period_end_date_4i').val(tmp < 10 ? '0' + tmp.toString() : tmp);
      tmp = dt.getMinutes();
      $('#uc_period_begin_date_5i, #uc_period_end_date_5i').val(tmp < 10 ? '0' + tmp.toString() : tmp);

      $('#uc_period_turned_on').prop('disabled', false);
    }
  });

  if ($('#uc_no_time').prop('checked')) {
    $('#uc_no_time').trigger('change');
  }

  $(document.body).on('click', '.uc-warn-close', function () {
    $('#uc_warn_note').hide();
    localStorage['uc_warn_state'] = 'closed';
  });

  $(document.body).on('change', '.uc-controller', function(event) {
    var $select = $(event.target);
    var $actions_select = $select.siblings('select').first();
    var controller = $select.val();
    var action_options = $('#all_actions').find('optgroup[label='+ controller +']').children().clone();
    $actions_select.children().filter(function(index){
      return this.innerHTML !== "&nbsp;";
    }).remove();
    $actions_select.val("");
    $actions_select.append(action_options);
  });

  RMPlus.Tech.show_warn_if_needed();
});
