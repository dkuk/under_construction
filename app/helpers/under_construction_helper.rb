module UnderConstructionHelper
  def uc_format_time_to_date(date, options={})
    format_time(date, true, options[:user]).split(' ')[0]
  end

  def uc_format_time_to_datetime(date, options={})
    format_time(date, true, options[:user]).split(' ')[1]
  end
end