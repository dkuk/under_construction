class UcPeriod < ActiveRecord::Base
  belongs_to :user
  has_many :uc_restrictions, dependent: :destroy

  attr_accessible :user_id, :custom_message, :turned_on, :notify

  accepts_nested_attributes_for :uc_restrictions, allow_destroy: true,
                                                  reject_if: lambda {|attrs| attrs[:controller].blank? || attrs[:action].blank? }
  attr_protected :id

  def begin_date
    if read_attribute(:begin_date)
      read_attribute(:begin_date).localtime
    else
      read_attribute(:begin_date)
    end
  end

  def end_date
    if read_attribute(:end_date)
      read_attribute(:end_date).localtime
    else
      read_attribute(:end_date)
    end
  end

  def active?(time=nil)
    time ||= DateTime.now
    turned_on && begin_date <= time && end_date > time
  end

  def planning?(time=nil)
    time ||= DateTime.now
    turned_on && begin_date > time
  end

  def manual_off?(time=nil)
    time ||= DateTime.now
    !turned_on && begin_date <= time && end_date > time
  end


  def notify_users
    return unless Redmine::Plugin::registered_plugins.include?(:redmine_sender)
    text_message = txt_to_notify
    return if text_message.blank?

    RsSender.send_message(via: 'xmpp',
                          body: txt_to_notify,
                          users: User.active.to_a,
                          send_now: true
    )
  end

  def txt_to_notify(add_body=true)
    msg = nil
    time = DateTime.now

    if active?(time)
      msg = l(:label_dear_colleagues)+"\n\n"
      msg << l(:infromation_technical_operations_emergency, time_end: format_time(end_date))+"\n\n"
      msg << custom_message if add_body && custom_message && custom_message != ''
    elsif planning?(time)
      msg = l(:label_dear_colleagues)+"\n\n"
      msg << l(:information_technical_operations_planing_xmpp, time_start: format_time(begin_date), time_end: format_time(end_date))+"\n\n"
      msg << custom_message if add_body && custom_message && custom_message != ''
    # elsif manual_off?(time)
    #   msg = l(:label_dear_colleagues)+"\n\n"
    #   msg << l(:infromation_technical_operations_ends)
    else
      msg = custom_message if add_body
    end
    msg
  end
end