RedmineApp::Application.routes.draw do

  resources :uc_periods do
    collection do
      get 'under_construction'
      get 'add_restriction'
      post 'preview'
    end
  end

  match 'uc_restrictions/destroy' => 'uc_restrictions#destroy', via: :delete

  resources :uc_browser_restrictions

end