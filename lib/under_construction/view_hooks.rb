module UnderConstruction
  module UnderConstruction
    class Hooks  < Redmine::Hook::ViewListener
      render_on(:view_layouts_base_body_top, :partial => 'hooks/under_construction/view_layouts_base_body_top')
    end
  end
end