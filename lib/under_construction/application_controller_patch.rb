module UnderConstruction
  module ApplicationControllerPatch
    def self.included(base)
      base.send(:include, InstanceMethods)

      base.class_eval do
        before_action :check_under_construction
        before_action :check_browser_restrictions
      end
    end

    module InstanceMethods
      def check_under_construction
        return true if User.current.admin? || params[:controller] == 'account'
        @uc_period = UcPeriod.order('begin_date desc').first
        is_under_construction = false
        if @uc_period && @uc_period.active? && !(controller_name == 'avatars' && action_name == 'get_ldap_avatar')
          if @uc_period.uc_restrictions.any?
            is_under_construction = UcRestriction.where("#{UcRestriction.table_name}.uc_period_id = :uc_period_id AND
                                                        (#{UcRestriction.table_name}.controller IN ('all', :controller_name) AND
                                                         #{UcRestriction.table_name}.action IN ('all', :action_name))",
                                                        {uc_period_id: @uc_period.id, controller_name: controller_name, action_name: action_name}).any?
          else
            is_under_construction = true
          end
        end
        if is_under_construction
          @users = [@uc_period.user]
          @rm_layout = true
          render 'uc_periods/under_construction', layout: request.xhr? ? false : 'rmp_layout'
        end
      end

      def check_browser_restrictions
        user_browser = Browser.new(ua: request.env['HTTP_USER_AGENT'], accept_language: 'en-us')
        unsupported_browser = false
        if Setting.plugin_under_construction.is_a?(Hash) && Setting.plugin_under_construction['enable_restrictions'] && !(controller_name == 'avatars' && action_name == 'get_ldap_avatar')
          UcBrowserRestriction.where(name: user_browser.id.to_s).each do |r|
            case r.condition
            when ''
              unsupported_browser = true
              break
            when '='
              if user_browser.version.to_f == r.version.to_f
                unsupported_browser = true
                break
              end
            when '<'
              if user_browser.version.to_f < r.version.to_f
                unsupported_browser = true
                break
              end
            when '>'
              if user_browser.version.to_f > r.version.to_f
                unsupported_browser = true
                break
              end
            end
          end
        end

        if unsupported_browser
          Rails.logger.error "\n\n ~~~~[ BROWSER RESTRICTION OCCURS ]~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"+
                             "\n Time: #{Time.now}"+
                             "\n User: #{User.current.name}  (user_id=#{User.current.id})  was try to open RM from ip=#{request.remote_ip}"+
                             "\n Browser: #{user_browser.name} v.#{user_browser.version}"+
                             "\n ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n"
          @users = Setting.plugin_under_construction['responsible_ids'].is_a?(Array) ? User.where(id: Setting.plugin_under_construction['responsible_ids']).active : []
          @rm_layout = true
          render 'uc_browser_restrictions/unsupported_browser', layout: request.xhr? ? false : 'rmp_layout'
        end
      end
    end

  end
end